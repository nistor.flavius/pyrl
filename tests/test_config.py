import unittest

from config.app_config import Config
from validator_collection import validators


class TetsConfig(unittest.TestCase):

    config = Config()

    def test_load_configuration(self):
        self.assertIsInstance(self.config.load_configuration(), (dict,), "Invalid format data type is not dict")

    def test_get_depth(self):
        self.assertIsInstance(self.config.get_depth(), (str, int), "Invalid format data type is not dict")

    def test_download_size(self):
        self.assertIsInstance(self.config.get_download_size(), (int, str), "Invalid format data type is not dict")

    def test_download_path(self):
        self.assertIsInstance(self.config.get_download_path(), (str,), "Invalid format data type is not dict")

    def test_get_crawl_url(self):
        self.assertIsInstance(self.config.get_crawl_url(), (str,), "Invalid format data type is not dict")

    def test_1_set_crawl_url(self):
        current_url = self.config.get_crawl_url()
        self.assertIsInstance(current_url, (str,), 'Invalid type for CRAWL_URL')
        current_url = validators.url(current_url)
        if current_url != 'http://python.org':
            self.config.set_crawl_url('http://python.org')
        else:
            self.config.set_crawl_url('http://nvidia.com')
        new_url = self.config.get_crawl_url()
        self.assertNotEqual(current_url, new_url, "No changes made to the CRAWL_URL value")

    def test_9_write_file(self):
        current_val = self.config.get_depth()
        self.assertIsInstance(current_val, (int,), 'Invalid type for CRAWL_DEPTH')
        if current_val != 1:
            self.config.write_file(1, 1)
        else:
            self.config.write_file(2, 2)
        new_val = self.config.get_depth()
        self.assertNotEqual(current_val, new_val, "No changes made to the config file")


if __name__ == '__main__':
    unittest.main()
