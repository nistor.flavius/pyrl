import unittest

from main.app import App


class TetsApp(unittest.TestCase):

    def test_select_change_configuration_parameter(self):
        self.assertTrue('Config parameters could not be displayed')

    def test_view_configuration(self):
        self.assertTrue('Error displaing configuration from file')

    def test_change_url(self):
        self.assertTrue('Error rendering Start crawling menu')

    def test_change_url_menu(self):
        self.assertTrue('Invalid URL provided !')

    def test_format_menu(self):
        self.assertIsInstance(App().format_menu(['X', 'test']), (str,), "Invalid format for input data")

    def test_change_crawl_depth(self):
        self.assertTrue('Invalid value provided for parameter (CRAWL_DEPTH)')

    def test_change_storage_size(self):
        self.assertTrue('Invalid value provided for parameter (STORAGE_SIZE)')

    def test_change_config(self):
        self.assertTrue("Invalid parameter values")

    def test_show_return_exit(self):
        self.assertTrue('Menu data invalid')


if __name__ == '__main__':
    unittest.main()
