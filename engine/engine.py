import scrapy
import datetime
from os import system
from config import app_config
from urllib.parse import urlparse


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Engine(scrapy.Spider, metaclass=Singleton):
    """Spider class, it crawls ..."""
    name = 'ItemCrawler101'
    dwn_path = None
    custom_settings = {
        'DEPTH_LIMIT': app_config.Config().get_depth(),
        'DOWNLOAD_MAXSIZE': app_config.Config().get_download_size(),
        'REDIRECT_MAX_TIMES': app_config.Config().get_depth(),
        'DOWNLOAD_HANDLERS': {'ftp': None}
    }

    def __init__(self, *args, **kwargs):
        super(Engine, self).__init__(*args, **kwargs)
        # set crawler url from config file, I do it here so that the value is updated from the config file
        self.start_urls = [app_config.Config().get_crawl_url()]

        # set the folder name so we can find the donloaded content
        self.dwn_path = f"{self.start_urls[0].split('//')[-1]}_{datetime.datetime.now().hour}_{datetime.datetime.now().minute}"

        # create folder inside the downloads folder
        system(f"mkdir {app_config.Config().get_download_path()}/{self.dwn_path}")

        # set allowed domains
        self.allowed_domains = [urlparse(self.start_urls[0]).netloc]

        # update the spider settings, from the config file
        self.custom_settings = {
            'DEPTH_LIMIT': app_config.Config().get_depth(),
            'DOWNLOAD_MAXSIZE': app_config.Config().get_download_size(),
            'REDIRECT_MAX_TIMES': app_config.Config().get_depth(),
            'DOWNLOAD_HANDLERS': {'ftp': None}
        }

    def parse(self, response):
        """parse method, default for spider, parses the response and writes it to a file"""
        filename = f"{app_config.Config().get_download_path()}/{self.dwn_path}/{response.url.split('/')[-1]}.html"
        with open(filename, 'wb') as f:
            f.write(response.body)
        # only get the first 25 links otherwise things get out of hand really fast
        for href in response.xpath('//a/@href').getall()[0:app_config.Config().get_links_number()]:
            yield scrapy.Request(response.urljoin(href), self.parse)

