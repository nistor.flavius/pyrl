import sys, getopt
from main.app import App

def main():
    debug_mode = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hdc:", ['help', 'debug=', 'config_file='])
    except getopt.GetoptError as e:
        print('start.py --debug=<boolean>')
        sys.exit()
    for opt, arg in opts:
        if opt in ['-h', '--help']:
            print('Usage: start.py [options]')
            print('\n')
            # print('\t  --config=config_file.toml \t specify a configuration file to use')
            print('\t  --debug=<boolean> \t run with debugging mode enabled')
            print('\t  --help \t show this help')
            print('\n')
            sys.exit()
        elif opt in ('-d', '--debug'):
            debug_mode = arg
    App(debug_mode).run_app()


if __name__ == '__main__':
    main()
