# Instructions

## Requirements

Python 3.6 or higher

Scrapy 1.6

toml

validator-collection

## How to 

Clone the project with:

```
git clone git@gitlab.com:nistor.flavius/pyrl.git
```

or 

```
git clone https://gitlab.com/nistor.flavius/pyrl.git
```

Navigate to the root of the project and run `pip` to install all the requirements with:

```
pip install -r requirements.txt
```

To start run:

```
python start.py
```

for debugging use:

```bash
python start.py --debug=True
```

## Description

Once started the application presents the user with a small menu as follows:

```bash
[S] Start crawling
[V] View configuration
[E] Edit configuration
[X] Exit

```

The first entry `[S]` allows the user to configure the URL that he/she wants to crawl and start the crawling process.

The second entry, `[V]`, allow the user to see the current configuration, the `[E]` entry allows the user to change the configuration parametrs for crawling depth and storage size.

`[X]` stands for `Exit` and it does that, it exits the application.

## Crawling  process

Once initiated the spider will start crawling the given URL. The CRAWL_DEPTH and STORAGE_SIZE will be taken into account
while crawling, thus if a link is beyond the depth parameter setting, the spider will not follow that link, also if a resource is larger than 
the storage size parameter the spider will stop.


## Tests

To run the tests, from the root directory run:

```
python -m unittest
```


