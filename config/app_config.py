import toml
from validator_collection import validators


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Config(metaclass=Singleton):
    """Configuration class
    Manages the configuration file config.toml.
    """
    def __init__(self, filename='./config/config.toml'):
        # set the filename or throw error if filename is empty
        if filename is not None:
            self.config_file = filename
        else:
            raise ValueError('Configuration file must be provided')

    def load_configuration(self):
        """Load the configuration from TOML file"""
        return toml.load(self.config_file)

    def get_depth(self):
        """Reads the crawl depth value from the configuration file"""
        return int(self.load_configuration()['main']['CRAWL_DEPTH'])

    def get_download_size(self):
        """Reads the download size from the configuration file"""
        return int(self.load_configuration()['main']['STORAGE_SIZE'])

    def get_links_number(self):
        """Reads the links to follow number configuration file"""
        return int(self.load_configuration()['main']['LINKS_NUMBER'])

    def get_download_path(self):
        """Reads the downloads folder path from the configuration file"""
        return self.load_configuration()['paths']['DOWNLOADS']

    def get_crawl_url(self):
        """Reads the URL for crawling from the configuration file"""
        return self.load_configuration()['url']['URL']

    def set_crawl_url(self, p1):
        """Write the crawl URL to the configuration file"""

        # check parameter has value
        if p1 is None or p1 == '':
            raise ValueError('Value required for URL, value must be a valid URL')

        # validate URL
        valid_url = validators.url(p1)

        # read exisiting configuration into variable
        exisitng_data = self.load_configuration()

        # update variable content with the new URL
        exisitng_data.update({'url': {'URL': valid_url}})

        # open configuration file
        with open(self.config_file, 'w+') as toml_file:
            # write data to file
            toml.dump(exisitng_data, toml_file)
        return toml_file

    def write_file(self, p1, p2, p3):
        """Write depth and storage config values to config file"""

        # check values are integers
        if not isinstance(int(p1), int):
            raise ValueError('Value required for CRAWL_DEPTH, value must be an integer')
        if not isinstance(int(p2), int):
            raise ValueError('Value required for STORAGE_SIZE, value must be an integer')
        if not isinstance(int(p3), int):
            raise ValueError('Value required for LINKS_NUMBER, value must be an integer')

        # read crawl URL from config file
        url = self.get_crawl_url()

        #read download path from config file
        dwn_path = self.get_download_path()

        # open config file
        with open(self.config_file, 'w+') as toml_file:
            # write new configuration to file
            toml.dump({'main': {'CRAWL_DEPTH': p1, 'STORAGE_SIZE': p2, 'LINKS_NUMBER': p3},
                       'url': {'URL': url},
                       'paths': {'DOWNLOADS': dwn_path}}
                      , toml_file)
        return toml_file
