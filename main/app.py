import sys
from os import name, system
from config import app_config
from validator_collection import validators
from scrapy.crawler import CrawlerProcess

from engine.engine import Engine


class App:
    """Application class, main CLI menus and menu logic"""

    running = True # bool variable to set running status - main loop
    config = app_config.Config().load_configuration()['main'] # configuration variable
    crawl_depth = app_config.Config().get_depth() # configuration for depth
    storage_size = app_config.Config().get_download_size() # configuration for download size
    URL = app_config.Config().get_crawl_url() # configuration for URL
    config_changed = False # bool variable for configuration status
    debug = 'False'
    links_number = app_config.Config().get_links_number()

    # colors from the internet
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def __init__(self, debug_parameter):
        self.debug = debug_parameter

    def run_app(self):
        """Main function, displays the main menu and starts the main loop. If user input is provided will display
        the proper submenu"""
        self.display_menu(None)

        # main loop
        while self.running:
            user_input = input(f'{self.BOLD}Enter selection: {self.ENDC}')
            self.display_menu(user_input.upper())
        sys.exit()

    def change_url_menu(self):
        """Generate the interface for URL changing"""
        self.show_return_exit()
        url_value = input(f'{self.BOLD}Enter the desired URL for crawling: {self.ENDC} \n')
        self.URL = validators.url(url_value)
        app_config.Config().set_crawl_url(self.URL)
        self.display_menu('S')
        return True

    def change_url(self):
        """Menu for URL changing"""
        crawl_menu = {
            'U': 'Enter URL',
            'C': 'Crawl',
        }
        if self.URL is not None:
            print(f'{self.HEADER}URL value: {self.URL}{self.ENDC}\n')
        for r in crawl_menu.items():
            if r[0] == 'C' and self.URL is not None:
                print(self.format_menu(r))
            if r[0] != 'C':
                print(self.format_menu(r))
        self.show_return_exit()
        # if we get here everithing is fine :)
        return True

    def start_crawling(self):
        """Starts the crawling process"""
        print(f'{self.HEADER}Crawling {self.URL}{self.ENDC}')
        process = CrawlerProcess({
            'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
        })
        # I can add another spider to the crawler and then it would run in parallel
        # but I did not manage to create another spider dynamically
        process.crawl(Engine())
        process.start()
        self.display_menu('finish')

    def view_configuration(self):
        """Displays the current configuration depth and download size"""
        print(f'{self.HEADER}Current configuration {self.ENDC}\n')
        read_configuration = app_config.Config().load_configuration()['main']
        for t in read_configuration.items():
            print(self.format_menu(t))
        self.show_return_exit()
        print(f'{self.OKGREEN}Storage size is expresed in bytes.{self.ENDC}')
        # if read configuration does not fail we are fine
        return True

    def select_change_configuration_parameter(self):
        if self.config is None:
            return False
        change_config_menu = {f"{idx}": t for idx, t in enumerate(self.config)}
        print(f'{self.HEADER}Select the configuration setting you want to change: {self.ENDC}\n')
        for r in change_config_menu.items():
            print(self.format_menu(r))
        self.show_return_exit()
        return True

    def change_crawl_depth(self):
        """Display the change depth parameter interface"""
        self.show_return_exit()
        self.crawl_depth = input(f'{self.BOLD}Enter value for CRAWL_DEPTH: {self.ENDC}')
        try:
            int(self.crawl_depth)
            self.change_config(self.crawl_depth, self.storage_size, self.links_number)
            self.config_changed = True
            self.display_menu(None)
        except BaseException as e:
            print(e)
            self.change_crawl_depth()
        return True

    def change_storage_size(self):
        """Display the change download size parameter interface"""
        self.show_return_exit()
        self.storage_size = input(f'{self.BOLD}Enter value for STORAGE_SIZE ( ex. 20 for 20 MB): {self.ENDC}')
        try:
            int(self.storage_size)
            self.change_config(self.crawl_depth, int(self.storage_size) * 1024 * 1024, self.links_number)
            self.config_changed = True
            self.display_menu(None)
        except BaseException as e:
            print(e)
            self.change_storage_size()
        return True

    def change_links_number(self):
        """Display the change links number parameter interface"""
        self.show_return_exit()
        self.links_number = input(f'{self.BOLD}Enter value for LINKS_NUMBER: {self.ENDC}')
        try:
            int(self.links_number)
            self.change_config(self.crawl_depth, self.storage_size, self.links_number)
            self.config_changed = True
            self.display_menu(None)
        except BaseException as e:
            print(e)
            self.change_links_number()
        return True

    def finish(self):
        """Displays a message after the crawler efinishes and stops the main loop"""
        print(f"{self.HEADER}Crawling has finished, files downloaded to {app_config.Config().get_download_path()}{self.ENDC}")
        self.running = False

    def exit_app(self):
        """Stop the main loop"""
        print(f'{self.OKGREEN}Bye bye!{self.ENDC}')
        self.running = False

    @staticmethod
    def change_config(cd, ss, ln):
        """Change the config parameters via Config class"""
        if cd is not None and ss is not None and ln is not None:
            app_config.Config().write_file(cd, ss, ln)
            return True
        return False

    def show_return_exit(self):
        """Displays the menu for return and exit options"""
        return_exit_menu = {
            'R': 'Return to main menu',
            'X': 'Exit'
        }
        print('\n')
        for r in return_exit_menu.items():
            print(self.format_menu(r))
        print('\n')
        return True

    def format_menu(self, record):
        """Formats the options inside the menu - color and ..."""
        return f"[{self.OKBLUE}{record[0]}{self.ENDC}] : {record[1]}"

    def display_menu(self, option=None):
        """Displays the requested menu via the option parameter"""
        if not self.debug == 'True':
            if name == 'nt':
                system('cls')
            else:
                system('clear')
        main_menu = {
            'S': 'Start crawling',
            'V': 'View configuration',
            'E': 'Edit configuration',
            'X': 'Exit'
        }

        print('=====================================================================================================\n')

        if option not in ['0', 'X', 'C', '1', 'S', 'V', 'E', 'U', 'finish', '2']:
            print(f'{self.HEADER}Select an option from bellow:{self.ENDC} \n')
            for r in main_menu.items():
                print(self.format_menu(r))
            if self.config_changed:
                print(
                    f"\n{self.WARNING}Please restart application in order for the new settings to take effect.{self.ENDC}")

        switch = {
            '': self.display_menu,
            'U': self.change_url_menu,
            'S': self.change_url,
            'C': self.start_crawling,
            'V': self.view_configuration,
            'E': self.select_change_configuration_parameter,
            '0': self.change_crawl_depth,
            '1': self.change_storage_size,
            'X': self.exit_app,
            'R': self.display_menu,
            'finish': self.finish,
            '2': self.change_links_number,
        }
        if option is not None and option in ['0', 'X', 'C', '1', 'S', 'V', 'E', 'U', 'finish', '2']:
            switch[option]()

        print('\n=====================================================================================================')




